Rails.application.routes.draw do
  require 'api_constraints'
  root to: 'restraunts#index'
  resources :restraunts
  devise_for :users
  resources :users
  resources :categories
  post 'likes/give', as: 'site_like'
  resources :comments, except: [:create]
  namespace :api do
    scope module: :v1, constraints: ApiConstraints.new(version: 1,
                                                       default: true) do
      resources :restraunts
      resources :categories
      resources :dishes
      post 'likes/give', as: 'like'
      get 'users/log_in'
      get 'users/log_out'
    end
  end
end
