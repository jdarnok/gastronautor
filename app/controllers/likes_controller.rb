class LikesController < ApplicationController
	before_filter :authenticate_user!
	def give
		restraunt = Restraunt.find(params[:restraunt_id])
		restraunt.like(current_user)
		respond_to do |format|
			format.js
			format.json { head :no_content }
		end
	end
end
