class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update]
  def create
    @restraunt = Restraunt.find(params[:restraunt_id])

    respond_to do |format|
      if @restraunt.save
    format.html { redirect_to @restraunt, notice: 'Category was successfully created.' }
    format.json { render 'show', status: :created, location: @restraunt }
  else
    format.html { render 'new' }
    format.json { render json: @restraunt.errors, status: :unprocessable_entity }
  end
    end
  end

  def update
  end

  def index
    @comments = Comment.all
  end

  def new
    @comment = Comment.new
  end

  def edit
  end

  def show
  end

  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_comment
    @comment = Comment.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:description, :commentable_id,
                                    :commentable_type, :user_id
                                    )
  end
end
