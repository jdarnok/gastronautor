class RestrauntsController < ApplicationController
  before_action :set_restraunt, only: [:show, :edit, :update, :destroy]
# TODO add google map to view
  def index
    @restraunts = Restraunt.includes(:categories, :likes).all
  end

  def show
    authorize @restraunt
    @comment = Comment.new
  end

  def create
    @restraunt = Restraunt.new(restraunt_params)

    respond_to do |format|
      if @restraunt.save
    format.html { redirect_to @restraunt, notice: 'Category was successfully created.' }
    format.json { render action: 'show', status: :created, location: @restraunt }
  else
    format.html { render action: 'new' }
    format.json { render json: @restraunt.errors, status: :unprocessable_entity }
  end
    end
  end

  def update
    respond_to do |format|
      if @restraunt.update(restraunt_params)
        format.html { redirect_to @restraunt, notice: 'Restraunt was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @restraunt.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @restraunt.destroy
     respond_to do |format|
     format.html { redirect_to categories_url }
     format.json { head :no_content }
   end
  end

  def edit
  end

  def new
    @restraunt = Restraunt.new
  end
  private
  # Use callbacks to share common setup or constraints between actions.
  def set_restraunt
    @restraunt = Restraunt.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def restraunt_params
    params.require(:restraunt).permit(:name, :description, :location)
  end
end
