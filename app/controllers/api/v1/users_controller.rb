class Api::V1::UsersController < ApplicationController
  include TokenAuthentication
  before_filter :authenticate_user_from_token!, except: [:log_in]
  before_filter :authenticate_user!, except: [:log_in]
  def log_out
    if current_user
      sign_out current_user
      render json: { message: "Wylogowano" }
    else
      render json: { message: 'blad' }
  end
end

  def log_in
    user = User.find_by(email: request.headers["X-USER-EMAIL"])
    if user.valid_password?(request.headers["X-USER-PASSWORD"])
      sign_in user
    render json: {auth_token: "#{current_user.authentication_token}"}
  else
    render json: {message: "bledne haslo"}
  end
end


end
