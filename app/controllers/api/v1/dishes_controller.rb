class Api::V1::DishesController < ApplicationController
  def index
    @dishes = Dish.where(category_id: params[:categoryID])
    render json: @dishes
  end
end
