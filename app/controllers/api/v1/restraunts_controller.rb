class Api::V1::RestrauntsController < ApplicationController
  respond_to :json
  def index
    @restraunts =  Restraunt.includes(categories: [:dishes]).all
    render json: @restraunts
  end
  def show
    @restraunt = Restraunt.find(params[:id])
    render json: @restraunt
  end
end
