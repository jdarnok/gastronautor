class Api::V1::LikesController < ApplicationController
  skip_before_filter :verify_authenticity_token, if: :json_request?
  include TokenAuthentication
  def give  
    restraunt = Restraunt.find(params[:restraunt_id])
    restraunt.like(current_user)
    render json: {message: "OK #{current_user.email}"}
  end
end
