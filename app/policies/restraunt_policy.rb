class RestrauntPolicy < ApplicationPolicy
  attr_reader :user, :restraunt

  def initialize(user, restraunt)
    @user = user
    @restraunt = restraunt
  end

  def show?
    user.user? || user.admin?
  end

  def edit?
      user.admin?
  end

  def update?
    user.admin?
  end

  def new?
    user.admin?
  end

  def destroy?
    user.admin?
  end

end
