module Likeable
  extend ActiveSupport::Concern
  included do
    has_many :likes, as: :likeable
  end

  def like(user)
    like = Like.find_or_initialize_by(likeable_id: self.id, likeable_type: self.class.name, user_id: user.id)
    unless like.persisted?
      like.save
    end
    # Like.create(likeable_id: self.id, likeable_type: self.class.name, user_id: user.id)
  end

end
