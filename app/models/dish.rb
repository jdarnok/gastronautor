class Dish < ActiveRecord::Base
  belongs_to :category
  include Commentable
  include Likeable
  validates_presence_of :category, :name, :price, :description

end
