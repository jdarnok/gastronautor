class Category < ActiveRecord::Base
  belongs_to :restraunt
  has_many :dishes

  validates_presence_of :name, :restraunt

end
