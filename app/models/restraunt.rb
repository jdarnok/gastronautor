class Restraunt < ActiveRecord::Base
  has_many :categories
  include Likeable
  include Commentable
  validates_presence_of :description, :location, :name
  validates :name, uniqueness: true
end
