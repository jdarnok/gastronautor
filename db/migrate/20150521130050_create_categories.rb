class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.references :restraunt, index: true
      t.string :name

      t.timestamps null: false
    end
    add_foreign_key :categories, :restraunts
  end
end
