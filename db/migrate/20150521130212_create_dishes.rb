class CreateDishes < ActiveRecord::Migration
  def change
    create_table :dishes do |t|
      t.string :name
      t.float :price
      t.string :description
      t.references :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :dishes, :categories
  end
end
