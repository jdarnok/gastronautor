# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email

10.times do
  restraunt = Restraunt.create(name: Faker::Lorem.word,
                               description: Faker::Lorem.sentence)
  5.times do
    category = Category.create(restraunt: restraunt,
                               name: Faker::Lorem.word)
    5.times do
      Dish.create(name: Faker::Lorem.word, price: Faker::Number.number(2),
                  category: category, description: Faker::Lorem.sentence)
    end
  end
end
