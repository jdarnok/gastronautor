FactoryGirl.define do
  factory :restraunt do
    name { Faker::Commerce.color }
    description 'MyString'
    location 'MyString'
  end

  factory :invalid_restraunt, class: Restraunt do
    name nil
    description 'Mysdasd'
    location 'asdas'
  end
end
