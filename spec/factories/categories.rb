FactoryGirl.define do
  factory :category do
    restraunt
    name { Faker::Commerce.color }
  end
  factory :invalid_category, class: Category do
    restraunt nil
    name nil
  end
end
