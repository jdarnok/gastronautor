FactoryGirl.define do
  factory :comment do
    description "MyString"
    user
    trait :restraunt_comment do
      association :commentable, factory: :restraunt
    end
    trait :dish_comment do
      association :commentable, factory: :dish
    end
end
    factory :invalid_comment, class: Comment do
      description nil
      user nil
      trait :dish_comment do
        association :commentable, factory: :dish
      end
    end
end
