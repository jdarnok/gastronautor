FactoryGirl.define do
  factory :like do
    trait :restraunt_like do
      association :likeable, factory: :restraunt
    end
    trait :dish_like do
      association :likeable, factory: :dish
    end
  end

end
