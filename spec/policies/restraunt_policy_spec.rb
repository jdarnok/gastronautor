describe RestrauntPolicy do
  subject  {RestrauntPolicy}
  before do
    @user = build_stubbed(:user)
    @admin = build_stubbed(:user, :admin)
    @restraunt = build_stubbed(:restraunt)
  end
  permissions :show? do
    it 'grants access if user has user role ' do
      expect(subject).to permit(@user,@restraunt)
    end
    it 'grants access if user has admin role' do
      expect(subject).to permit(@admin,@restraunt)
    end
  end
  permissions :edit? do
    it 'grants access if user has admin role' do
      expect(subject).to permit(@admin,@restraunt)
    end
    it 'denies access if user does not have admin role' do
      expect(subject).to_not permit(@user,@restraunt)
    end
  end
  permissions :update? do
    it 'grants access if user has admin role' do
      expect(subject).to permit(@admin,@restraunt)
    end
    it 'denies access if user does not have admin role' do
      expect(subject).to_not permit(@user,@restraunt)
    end
  end
  permissions :destroy? do
    it 'grants access if user has admin role' do
      expect(subject).to permit(@admin,@restraunt)
    end
    it 'denies access if user does not have admin role' do
      expect(subject).to_not permit(@user,@restraunt)
    end
  end

end
