require 'spec_helper'

shared_examples_for 'likeable' do
  let(:model) { create( described_class.to_s.underscore ) }

    it 'has likes' do

      expect { model.likes }.to_not raise_error
    end
    it 'like method returns Like class object as association ' do
      user = create(:user)
      model.like(user)
      expect(model.likes.length).to eq(1)
    end

    it "like method creates only one like per user for #{described_class.to_s}" do
      user = create(:user)
      model.like(user)
      model.like(user)
      expect(model.likes.length).to eq(1)
    end

end
