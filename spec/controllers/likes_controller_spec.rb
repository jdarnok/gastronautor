describe LikesController do
  context "users's first like" do
    before do
      user = create('user')
      allow(request.env['warden']).to receive(:authenticate!) { user }
      allow(controller).to receive(:current_user) { user }
    end
    context 'restraunt' do
      before do
        @restraunt = create(:restraunt)
      end
  it 'adds like to a restraunt' do
    expect { post :give, restraunt_id: @restraunt.id, format: 'js' }.to change(Like, :count)
    .by(1)
  end
  it 'renders :give template' do
    post :give, restraunt_id: @restraunt.id, format: 'js'
    expect(response).to render_template :give

  end
    end
  end
end
