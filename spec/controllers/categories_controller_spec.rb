  describe CategoriesController do
   describe 'GET #index' do
     it 'returns 200 HTTP status' do
       get :index
       expect(response).to have_http_status(200)
     end

     it 'populates the array of categories' do
       categories = create_list(:category, 2)
       get :index
       expect(assigns(:categories)).to match_array(categories)
     end

     it 'renders the :index template' do
       category = create(:category)
       get :index
       expect(response). to render_template :index
     end

   end
   describe 'GET #show' do
     before do
        @category = create(:category)
     end

     it 'assigns the requested category to @category' do
       get :show, id: @category
       expect(assigns(:category)).to eq @category
     end

     it 'renders the :show template' do
       get :show, id: @category
       expect(response).to render_template :show
     end
   end
   describe 'GET #edit' do
     before do
        @category = create(:category)
     end

     it 'assigns the requested category to @category' do
        get :edit, id: @category
        expect(assigns(:category)).to eq @category
     end

     it 'renders the :edit template' do
        get :edit, id: @category
        expect(response).to render_template :edit
     end
   end
   describe 'GET #new' do
     it 'assigns a new Category to @category' do
        get :new
        expect(assigns(:category)).to be_a_new(Category)
     end

     it 'renders the :new template' do
        get :new
        expect(response).to render_template :new
     end

   end
   describe 'POST #create' do
     before :each do
        @restraunt = create(:restraunt)
     end
     context 'with valid attributes' do
       it 'saves the new category in the database' do
         expect{
           post :create, category: attributes_for(:category,
                                                  restraunt_id: @restraunt.id)
         }.to change(Category, :count).by(1)
       end

       it 'redirects to categories#show' do
         post :create, category: attributes_for(:category,
                                                  restraunt_id: @restraunt.id)
         expect(response).to redirect_to category_path(assigns[:category])
       end
     end
     context 'with invalid attributes' do
       it 'does not save the new category in the database' do
         expect {
           post :create, category: attributes_for(:invalid_category)
         }.to_not change(Category, :count)
       end

       it 're-renders the :new template' do
         post :create, category: attributes_for(:invalid_category)
         expect(response).to render_template :new
       end
     end
   end
   describe 'PATCH #update' do
     before :each do
       @category = create(:category, name: "test_category")
     end
     context 'with valid attributes' do
       it 'locates the requested @category' do
         patch :update, id: @category, category: attributes_for(:category)
         expect(assigns(:category)).to eq(@category)
       end

       it "changes @category's attributes" do
         patch :update, id: @category, category: attributes_for(:category,
         name: "category")
         @category.reload
         expect(@category.name).to eq("category")
       end
       it 'redirects to the updated category' do
         patch :update, id: @category, category: attributes_for(:category)
         expect(response).to redirect_to @category
       end
     end
     context 'with invalid attributes' do

       it "does not change the category's name" do
         patch :update, id: @category, category: attributes_for(:category,
         name: nil)
         @category.reload
         expect(@category.name).to eq("test_category")
       end
       it "does not change the category's restraunt" do
         patch :update, id: @category, category: attributes_for(:category,
         restraunt: nil)
         @category.reload
         expect(@category.restraunt).to_not eq(nil)
       end

       it 're-renders the edit template' do
         patch :update, id: @category,
         category: attributes_for(:invalid_category)
         expect(response).to render_template :edit
       end
     end
   end
   describe 'DELETE #destroy' do
   end
 end
