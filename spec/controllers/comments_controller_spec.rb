describe CommentsController do
  describe 'GET #index' do
    it 'populates the array of comments' do
      comments = create_list(:comment, 2, :dish_comment)
      get :index
      expect(assigns(:comments)).to match_array(comments)
    end
    it 'renders the :index template' do
      get :index
      expect(response).to render_template :index
    end
  end
  describe 'GET #new' do
    it 'assigns a new Comment to @comment' do
      get :new
      expect(assigns(:comment)).to be_a_new(Comment)
    end

    it 'renders the :new template' do
      get :new
      expect(response).to render_template :new
    end
  end
  describe 'GET #show' do
    before do
      @comment = create(:comment, :dish_comment)
    end

    it 'assigns the requested comment to @comment' do
      get :show, id: @comment
      expect(assigns(:comment)).to eq @comment
    end

    it 'renders the :show template' do
      get :show, id: @comment
      expect(response).to render_template :show
    end
  end

  describe 'PATCH #update' do
    before :each do
      @comment = create(:comment, :dish_comment, description: 'test_comment')
    end
    context 'with valid attributes' do
      it 'locates the requested @comment' do
        patch :update, id: @comment, comment: attributes_for(:comment, :dish_comment)
        expect(assigns(:comment)).to eq(@comment)
      end

      it 'changes @comment attributes' do
        patch :update, id: @comment, comment: attributes_for(:comment, :dish_comment, description: 'a')
        @comment.reload
        expect(@comment.description).to eq('a')
      end

      it 'redirects to the updated comment' do
        patch :update, id: @comment, comment: attributes_for(:comment, :dish_comment)
        expect(response).to redirect_to @comment
      end
    end
  end
end
