describe RestrauntsController do
  describe 'Get #index' do
    it 'returns 200 HTTP status' do
      get :index
      expect(response).to have_http_status(200)
    end

    it 'populates the array of restraunts' do
      restraunts = create_list(:restraunt, 2)
      get :index
      expect(assigns(:restraunts)).to match_array(restraunts)
    end

    it 'renders the :index template' do
      restraunt = create(:restraunt)
      get :index
      expect(response).to render_template :index
    end
  end
  describe 'GET #show' do
    before do
      @restraunt = create(:restraunt)
      user = build_stubbed('user')
      allow(request.env['warden']).to receive(:authenticate!) { user }
      allow(controller).to receive(:current_user) { user }
    end

    it 'assigns the requested restraunt to @restraunt' do
      get :show, id: @restraunt
      expect(assigns(:restraunt)).to eq @restraunt
    end

    it 'renders the :show template' do
      get :show, id: @restraunt
      expect(response).to render_template :show
    end
  end
  describe 'GET #edit' do
    before do
      @restraunt = create(:restraunt)
    end

    it 'assigns the requested restraunt to @restraunt' do
      get :edit, id: @restraunt
      expect(assigns(:restraunt)).to eq @restraunt
    end

    it 'renders the :edit template' do
      get :edit, id: @restraunt
      expect(response).to render_template :edit
    end
  end
  describe 'GET #new' do
    it 'assigns a new Restraunt to @restraunt' do
      get :new
      expect(assigns(:restraunt)).to be_a_new(Restraunt)
    end

    it 'renders the :new template' do
      get :new
      expect(response).to render_template :new
    end
  end
  describe 'POST #create' do
    context 'with valid attributes' do
      it 'saves the restraunt in the database' do
        expect { post :create, restraunt: attributes_for(:restraunt) }
        .to change(Restraunt, :count).by(1)
      end

      it 'redirects to restraunts#show' do
        post :create, restraunt: attributes_for(:restraunt)
        expect(response).to redirect_to restraunt_path(assigns[:restraunt])
      end
    end
    context 'with invalid attributes' do
      it 'does not save the new restraunt in the database' do
        expect {
          post :create, restraunt: attributes_for(:invalid_restraunt)
        }.to_not change(Restraunt, :count)
      end
      it 're-renders the :new template' do
        post :create, restraunt: attributes_for(:invalid_restraunt)
        expect(response).to render_template :new
      end
    end
    describe 'PATCH #update' do
      before :each do
        @restraunt = create(:restraunt, name: "test_restraunt")
      end
      context 'with valid attributes' do
        it 'locates the requested @restraunt' do
          patch :update, id: @restraunt, restraunt: attributes_for(:restraunt)
          expect(assigns(:restraunt)).to eq(@restraunt)
        end
        it "changes @restraunt's attributes" do
          patch :update, id: @restraunt, restraunt: attributes_for(:restraunt,
                                                                  name: "res")
          @restraunt.reload
          expect(@restraunt.name).to eq("res")
        end
        it 'redirects to the updated restraunt' do
          patch :update, id: @restraunt, restraunt: attributes_for(:restraunt)
          expect(response).to redirect_to @restraunt
        end
      end
      context 'with invalid attributes' do
        it 'does not change the category name' do
            patch :update, id: @restraunt, restraunt: attributes_for(:restraunt,
                                                                    name: nil)
            @restraunt.reload
            expect(@restraunt.name).to eq("test_restraunt")
        end
      end
    end
  end
end
