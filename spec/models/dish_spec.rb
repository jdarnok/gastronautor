describe Dish do

  describe 'with valid data' do
    before do
      @dish = build_stubbed(:dish)
    end
    it 'has a valid factory' do
      expect(@dish).to be_valid
    end
    it 'has comments' do
      expect {@dish.comments}.to_not raise_error
    end
  end

  describe 'with invalid data' do
    it 'is invalid without name' do
      dish = build(:dish, name:nil)
      dish.save
      expect(dish.errors[:name].length).to eq(1)
    end
    it 'is invalid without price' do
      dish = build(:dish, price: nil)
      dish.save
      expect(dish.errors[:price].length).to eq(1)
    end
    it 'is invalid without description' do
      dish = build(:dish, description: nil)
      dish.save
      expect(dish.errors[:description].length).to eq(1)
    end
    it 'is invalid without category' do
      dish = build(:dish, category: nil)
      dish.save
      expect(dish.errors[:category].length).to eq(1)
    end
end

end
