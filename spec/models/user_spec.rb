describe User do

  describe 'with valid data' do
    before do
      @user = create(:user)
    end

    it 'has a valid factory' do
      expect(@user).to be_valid
    end
    it 'has a user role' do
      expect(@user.user?).to be true
    end
    it 'has an auth token' do
      expect(@user.authentication_token).to_not be_nil
    end
  end
  describe 'with invalid data' do
    it 'has invalid password_confirmation' do
      user = build(:user, password_confirmation: 'not_same_as_password')
      user.save
      expect(user.errors[:password_confirmation].length).to eq(1)
    end
    it 'has invalid email' do
      user = build(:user, email: 'invalidmail')
      user.save
      expect(user.errors[:email].length).to eq(1)
    end
    it 'does not allow duplicated email' do
      user = create(:user, email: 'test@example.com')
      user2 = build(:user, email: 'test@example.com')
      user2.save
      expect(user2.errors[:email].length).to eq(1)
    end
  end

end
