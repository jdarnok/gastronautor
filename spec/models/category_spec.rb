require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'with valid data' do
    before do
      @category = build(:category)
    end
    it 'has a valid factory' do
      expect(@category).to be_valid
    end
  end

  describe 'with invalid data' do
    it 'is invalid without name' do
      category = build(:category, name: nil)
      category.save
      expect(category.errors[:name].length).to eq(1)
    end
    it 'is invalid without a restraunt' do
      category = build(:category, restraunt: nil)
      category.save
      expect(category.errors[:restraunt].length).to eq(1)
    end
  end
end
