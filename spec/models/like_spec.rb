require 'rails_helper'

RSpec.describe Like, type: :model do
  describe ' valid data with restraunt association' do
    before do
      @like = build(:like, :restraunt_like)
    end
    it 'has a valid factory with restraunt association' do
      expect(@like).to be_valid
    end
  end
  describe 'valid data with dish association' do
    before do
      @like = build(:like, :dish_like)
    end
    it 'has a valid factory with dish association' do
      expect(@like).to be_valid
    end
  end

  describe 'invalid data' do
    it 'is invalid without association' do
      like = build(:like)
      like.save
      expect(like.errors[:likeable].length).to eq(1)
    end
  end
end
