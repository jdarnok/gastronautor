require 'rails_helper'

RSpec.describe Comment, type: :model do
  describe 'with restraunt association' do
    let(:comment) { build_stubbed(:comment, :restraunt_comment) }

    it 'has a valid factory' do
      expect(comment).to be_valid
    end
    it 'is invalid without user' do
      comment = build(:comment, :restraunt_comment, user:nil)
      comment.save
      expect(comment.errors[:user].length).to eq(1)
    end
    it 'is invalid without description' do
      comment = build(:comment, :restraunt_comment, description:nil)
      comment.save
      expect(comment.errors[:description].length).to eq(1)
    end
    it 'is invalid without commentable' do
      comment = build(:comment, :restraunt_comment, commentable:nil)
      comment.save
      expect(comment.errors[:commentable].length).to eq(1)
    end
end
    describe 'with dish association' do
      let(:comment) { build_stubbed(:comment, :dish_comment) }

      it 'has a valid factory' do
        expect(comment).to be_valid
      end

  end
end
