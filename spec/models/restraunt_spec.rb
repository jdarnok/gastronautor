require 'rails_helper'

RSpec.describe Restraunt, type: :model do

  it_behaves_like 'likeable'
  it_behaves_like 'commentable'

  describe 'with valid data' do
    before do
      @restraunt = build_stubbed(:restraunt)
    end
    it 'has valid factory' do
      expect(@restraunt).to be_valid
    end
    it 'has many comments' do
      expect { @restraunt.comments }.to_not raise_error
    end
  end
  describe 'with invalid data' do
    it 'is invalid without a name' do
      restraunt = build(:restraunt, name: nil)
      restraunt.save
      expect(restraunt.errors[:name].length).to eq(1)
    end
    it 'is invalid without description' do
      restraunt = build(:restraunt, description: nil)
      restraunt.save
      expect(restraunt.errors[:description].length).to eq(1)
    end
    it 'is invalid without location' do
      restraunt = build(:restraunt, location: nil)
      restraunt.save
      expect(restraunt.errors[:location].length).to eq(1)
    end
    it 'does not allow duplicated name' do
      restraunt = create(:restraunt, name: 'test_name')
      restraunt2 = build(:restraunt, name: 'test_name')
      restraunt2.save
      expect(restraunt2.errors[:name].length).to eq(1)
    end
  end

end
